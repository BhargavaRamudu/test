const http = require('http');
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const flash = require('connect-flash');
const session = require('express-session');

mongoose.connect('mongodb://localhost/appdb');

let db = mongoose.connection;

//Check connection
db.once('open', function(){
    console.log('Connected to mongodb');
})

//Check for db errors
db.on('error', function(err) {
    console.log(err);

})
const app = express();

let Article = require('./models/article');

// Load view Engine

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');



// const server = http.createServer(app);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'this is a secret',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: true}
}))

app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

app.get('/', function(req, res) {
    Article.find({}, function(err, articles) {
        if(err) {
            console.log(err);
        }else {
            res.render('index', {
                title: 'Hello',
                articles: articles
            });
        }
        
    })
    
})

app.get('/article/:id', function(req, res) {
    Article.findById(req.params.id, function(err, article) {
        res.render('article', {
            article: article
        })
    })
})


app.get('/article/edit/:id', function(req, res) {
    Article.findById(req.params.id, function(err, article) {
        res.render('edit_article', {
            article: article
        })
    })
})

// Update article

app.post('/article/edit/:id', function(req, res) {
    let article = {};
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;

    let query = { _id: req.params.id };

    Article.update(query, article, function(err) {
        if(err) {
            console.log(err);
            return;
        }else {
            res.redirect('/')
        }
    })
})


app.delete('/article/:id', function(req,res) {
    console.log('clicked the delete button');
    let query = {_id: req.params.id}

    Article.deleteOne(query, function(err) {
        if(err) {
            console.log(err);
        }
        res.send('Success');
        
    })
})

// Add route
app.get('/articles/add', function(req, res) {
    res.render('add_articles', {
        title: 'Add Articles'
    })
});

// Add submit post route
app.post('/articles/add', function(req,res) {
    let article = new Article();
    article.title = req.body.title;
    article.author = req.body.author;
    article.body = req.body.body;
    
    article.save(function(err) {
        if(err) {
            console.log(err);
            return;
        } else {
            res.redirect('/')
        }
    })
    
})



app.listen(3000, function(req, res) {
    console.log('Server has started on port 3000');
})

